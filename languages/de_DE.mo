��    0      �  C         (     )     ;     Q     V     h     t     }  =   �     �     �  	   �     �       /         P  	   j  -   t     �  1   �  W   �     >     C     H     W  	   \  
   f     q          �     �     �     �     �     �  F   �               4     D     L     ^  \   n     �     �     �     �       G  $     l	     �	     �	     �	  	   �	  
   �	      �	  8   �	      
     :
  	   I
     S
     h
  5   
     �
     �
  =   �
          3  [   M     �     �     �     �     �     �     �                    #     6  	   E     O  \   g     �     �     �     �            {   0     �  %   �     �     �  '   �                        '            .                "                 %      &   -       (          /                            ,             
                 #   $   !                                   *       +          0   	      )    Add widgets here. Author archive for %s Back Back to home page Back to top Comments Comments are closed. Continue reading<span class="screen-reader-text"> "%s"</span> Custom Footer Text Date and time Date only Display Settings Display author name Edit <span class="screen-reader-text">%s</span> For each article display: Full Text HTML enabled. Displays theme name by default. Internal Settings It looks like nothing was found at this location. It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Left Menu Newer comments Next Next post No sidebar Nothing Found Older comments Page Pages: Previous post Primary Color Primary Menu Publish Date Ready to publish your first post? <a href="%1$s">Get started here</a>. Right Search Results for: %s Secondary Color Sidebar Sidebar position: Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Summary That page can&rsquo;t be found. Theme: %1$s more noindex for categories and tags Project-Id-Version: Verdandi 1.7.0
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/verdandi
PO-Revision-Date: 2025-02-24 10:51+0100
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4.2
X-Domain: verdandi
 Füge hier Widgets hinzu. Archiv von %s Zurück Zurück zur Homepage Nach oben Kommentare Die Kommentare sind geschlossen. <span class="screen-reader-text">"%s" </span>Weiterlesen Individueller Footer Text Datum und Zeit Nur Datum Anzeigeeinstellungen Zeige den Autorennamen Bearbeiten <span class="screen-reader-text">%s</span> Artikel anzeigen als: Vollständiger Text HTML möglich. Zeigt standardmäßig den Namen des Themes an. Interne Einstellungen Es wurde nichts gefunden. Es scheint, dass wir nicht finden können, was Sie suchen.Vielleicht kann die Suche helfen. Links Menu Neuere Kommentare Weiter Nächster Beitrag Keine Seitenleiste Nichts gefunden Ältere Kommentare Seite Seiten: Vorheriger Beitrag Primäre Farbe Hauptmenu Veröffentlichungsdatum Sind Sie bereit, Ihren ersten Beitrag zu veröffentlichen? <a href="%1$s">Hier beginnen</a>. Rechts Suchergebnisse für: %s Sekundäre Farbe Seitenleiste Position der Seitenleiste: Zum Inhalt springen Es tut uns leid, aber Ihre Suchbegriffe wurden nicht gefunden.Bitte versuchen Sie es noch einmal mit anderen Suchbegriffen. Zusammenfassung Die Seite kann nicht gefunden werden. Theme: %1$s mehr noindex für Kategorien und Schlagworte 