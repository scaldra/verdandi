<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Verdandi
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
    <?php if ((is_archive()) && (get_theme_mod('noindex_tag_category', '1') === '1')) { ?>
        <meta name="robots" content="noindex, follow">
    <?php } ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'verdandi' ); ?></a>

	<header id="masthead" class="site-header">
        <div class="container">
            <div class="site-branding">
                <?php
                if ( function_exists( 'the_custom_logo' ) ) {
                    the_custom_logo();
                }

                if ( is_front_page() && is_home() ) : ?>
                    <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                <?php else : ?>
                    <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
                <?php
                endif;

                $description = get_bloginfo( 'description', 'display' );
                if ( $description || is_customize_preview() ) : ?>
                    <p class="site-description"><?php echo esc_html($description);  ?></p>
                <?php
                endif; ?>
            </div><!-- .site-branding -->

			<div class="header-image">
				<?php
                if ( get_header_image() ) {
                    the_header_image_tag();
                }
                ?>
			</div><!-- .header-image -->

            <?php if ( has_nav_menu( 'primary' ) ) : ?>

                <button id="menu-toggle" class="menu-toggle" aria-label="<?php esc_attr_e( 'Menu', 'verdandi' ); ?>"><span class="dashicons dashicons-menu-alt"></span></button>

                     <nav id="site-navigation" class="main-navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'verdandi' ); ?>">
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'primary',
                                'menu_class' => 'primary-menu',
                            )
                        );
                        ?>
                    </nav><!-- .main-navigation -->

            <?php endif; ?>

        </div><!-- .container -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
        <div class="container">
