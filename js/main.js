/**
 * File main.js.
 *
 * Theme's custom functions.
 */




/**
 * Scroll event.
 */
window.onscroll = function() {
    showToTopButton();
}

/**
 * Scroll event callback to display/hide back-to-top button.
 */
function showToTopButton() {
    var toTopBtn = document.getElementById('back-to-top');
    if (document.body.scrollTop > 500 ||
        document.documentElement.scrollTop > 500) {
        toTopBtn.classList.add('shown');

    } else {
        toTopBtn.classList.remove('shown');
    }
}
