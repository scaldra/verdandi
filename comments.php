<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Verdandi
 */


if (post_password_required()) {
    return;
}
?>

<div id="comments" class="comments-area">

    <?php if (have_comments()) : ?>

        <?php render_comments_title(); ?>
        <?php render_comment_list(); ?>
        <?php render_navigation(); ?>
        <?php render_closed_comments_message(); ?>

    <?php endif; ?>

    <?php comment_form(); ?>

</div><!-- #comments -->

<?php
/**
 * Renders the comments title block.
 */
function render_comments_title() {
    ?>
    <h2 class="comments-title">
        <?php esc_html_e('Comments', 'verdandi'); ?>
    </h2>
    <?php
}

/**
 * Renders the list of comments.
 */
function render_comment_list() {
    wp_list_comments(array(
        'style'       => 'ol',
        'avatar_size' => 32,
        'type'        => 'all',
        'short_ping'  => true
    ));
}

/**
 * Renders the navigation for older/newer comments.
 */
function render_navigation() {
    the_comments_navigation(array(
        'prev_text' => '< ' . __('Older comments', 'verdandi'),
        'next_text' => __('Newer comments', 'verdandi') . ' >',
    ));
}

/**
 * Renders a message if comments are closed.
 */
function render_closed_comments_message() {
    if (!comments_open()) {
        ?>
        <p class="no-comments"><?php esc_html_e('Comments are closed.', 'verdandi'); ?></p>
        <?php
    }
}
?>
