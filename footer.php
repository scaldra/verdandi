<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Verdandi
 */

?>

        </div><!-- .container -->
	</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="container">
        <a id="back-to-top" href="#page" aria-label="<?php esc_attr_e( 'Back to top', 'verdandi' ); ?>">
            <svg xmlns="http://www.w3.org/2000/svg" height="42" width="50"><path fill="white" d="M14.15 30.15 12 28l12-12 12 12-2.15 2.15L24 20.3Z"/></svg>
        </a>
        <div class="site-info">
            <?php
            $footer_text = get_theme_mod( 'footer_text', '' );
            if ( is_string( $footer_text ) && ! empty( $footer_text ) ) {
                echo wp_kses( $footer_text, wp_kses_allowed_html( 'post' ) );
            } else {

                printf(
                /* translators: Theme name */
                    esc_html__( 'Theme: %1$s', 'verdandi' ),
                    '<a href="' . esc_url( 'https://verdandi.scaldra.net' ) . '">Verdandi</a>'
                );
            }
            ?>
        </div><!-- .site-info -->
    </div><!-- .container -->
</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>
