=== Verdandi ===

Verdandi is a Wordpress theme specially designed for blogs. It features a clear structure and uses the  Atkinson Hyperlegible font to maximize readability. The theme supports various post types, including Standard, Aside, Video, Gallery, Quote, Image and Status.

The name Verdandi is derived from the Norne of the present. Verdandi writes down everything that happens - an ideal namesake for a blog that records stories and events.

* Mobile-first, Responsive Layout
* Custom Header
* Post Formats
* The GPL v3.0 or later license.

== Copyright ==

Verdandi WordPress Theme, Copyright 2024 Rolf Strathewerd
Verdandi is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.


Image for theme screenshot, "Circus in the fog" © 2024 by Rolf Strathewerd
License: Creative Commons Attribution-ShareAlike 4.0 International
License URL: https://creativecommons.org/licenses/by-sa/4.0/
Source: https://rolf.strathewerd.de/wp-content/uploads/2024/04/Circus_in_the_fog.jpg


Verdandi Theme bundles the following third-party resources:

Atkinson Hyperlegible Font
Copyright © 2020, Braille Institute of America, Inc. (https://www.brailleinstitute.org)
License: ATKINSON HYPERLEGIBLE FONT LICENSE
License URL: https://brailleinstitute.org/wp-content/uploads/2020/11/Atkinson-Hyperlegible-Font-License-2020-1104.pdf
Source: https://brailleinstitute.org/freefont



== Changelog ==
Version 1.7.2 (2025-02-25)
-------------------------
- Fix: default for noindex_tag_category should be off

Version 1.7.1 (2025-02-25)
-------------------------
- Fix: CSS for .widget

Version 1.7.0 (2025-02-24)
-------------------------
- New: Add color customization to theme settings
- New: Add noindex option for categories and tags
- Tweaks: Refactor comments handling, update styles, and optimize templates

Version 1.6.0 (2025-01-07)
-------------------------
- New: optional widgetarea in posts (between post and comments)
- some mini-tweaks

Version 1.5.1 (2024-10-10)
-------------------------
- Fix: align-wide on smaller screens

Version 1.5.0 (2024-10-09)
-------------------------
- Tweak: no border for images and videos on small screens
- New: Support for align-wide (wide and full) if no sidebar is active

Version 1.4.2 (2024-10-02)
-------------------------
- Fix: allow some HTML tags in footer

Version 1.4.1 (2024-09-27)
-------------------------
- Fix: Author URI


Version 1.4.0 (2024-09-16)
-------------------------
- Fix: burger menu initally not visible on small screens
- Tweak: use short_pings
- New: Sort pingbacks and trackbacks to the end of the comment block
- Tweak: scroll-to-top button now uses the same style as the other buttons
- Tweak: smaller width of navigation on mobile

Version 1.3.0 (2024-08-03)
-------------------------
- New: Support for the "link" post format
- Tweak: Better page navigation
- Tweak: Some minor CSS changes (nicer buttons, improved color scheme consistency)


Version 1.2.1 (2024-06-25)
-------------------------
- Fix: Underlines for links were not visible on IPad/IPhone


Version 1.2.0 (2024-06-12)
-------------------------
- Tweak: link decoration a little bit smaller
- Tweak: comment-area revamped


Version 1.1.1 (2024-05-21)
-------------------------
- Fix: missing translation


Version 1.1.0 (2024-04-29)
-------------------------
- Fix: Javascript for site navigation has to be in the footer to work correctly
- Tweak: new style for quotes
- Tweak: new style  for sticky posts
- Tweak: improved margin for image blocks
- Tweak: link to post formats
