<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Verdandi
 */

/*
if (!function_exists('verdandi_edit_post_link')) :
    function verdandi_edit_post_link()
    {
          edit_post_link(
                sprintf(
                    wp_kses(
                        __( 'Edit <span class="screen-reader-text">%s</span>', 'verdandi' ),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    get_the_title()
                ),
                '<span class="edit-link">',
                '</span>'
            );
    }
endif;
*/

if (!function_exists('verdandi_publisher_posted_on_date')) :
    /**
     * Prints HTML with meta information for the current post-date/time.
     */
    function verdandi_publisher_posted_on_date()
    {

        printf(
            '<span class="posted-on"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date dt-published" datetime="%3$s" itemprop="datePublished" pubdate="pubdate">%4$s %5$s</time></a></span>',
            esc_url( get_permalink() ),
            esc_attr( get_the_title() ),
            esc_attr( get_the_date(  ) ),
            esc_html( get_the_date()),
            'datetime' === get_theme_mod('display_datetime', 'date') ? esc_attr(get_the_time('H:i')) : ''
        );

    }
endif;

if (!function_exists('verdandi_post_icon')) :
    /**
     * Prints icon for post type
     */
    function verdandi_post_icon()
    {
        $path = '/type/' . get_post_format();
        $dashicon = 'dashicons-format-' . get_post_format();
        $label = get_post_format();
        switch (get_post_format()) {
            case 'aside':
            case 'image':
            case 'quote':
            case 'gallery':
            case 'status':
                break;
            case 'video':
                $dashicon = 'dashicons-video-alt2';
                break;
            case 'link':
                $dashicon = 'dashicons-format-links';
                break;
            default:
                $dashicon = 'dashicons-text';
                $label = 'text';
                $path = '';
        }
        printf(
            '<span class="post-type"><a href="%1$s" aria-label="%2$s"><span class="typeicon dashicons %3$s"></span></a></span>',
            esc_url(home_url($path)),
            esc_html($label),
            esc_html($dashicon)
        );
    }
endif;

if (!function_exists('verdandi_byline')) :
    /**
     * Prints HTML with meta information for the current post-date/time.
     */
    function verdandi_byline()
    {
        if (get_theme_mod('display_author', '1') === '1') {
        printf(
                '<span class="byline"><span class="author vcard"><a class="url fn n" href="%1$s">%2$s</a></span></span>',

                esc_url(get_author_posts_url(get_the_author_meta('ID'))),
                esc_html(get_the_author())
        );
    }
    }
endif;

if (!function_exists('verdandi_categories')) :
    /**
     * Prints HTML with meta information for the current post-date/time.
     */
    function verdandi_categories()
    {
        if ('post' === get_post_type()) {
            the_category(", ");
        }

    }
endif;

if (!function_exists('verdandi_entry_footer')) :
    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function verdandi_entry_footer()
    {
        if ('post' === get_post_type()) {
            if (is_single()) {
                $tags_list =  get_the_tag_list('', ', ');
                if ($tags_list) {
                    the_tags( '<span class="typeicon dashicons dashicons-tag"></span> <span class="tags-links">', ', ','</span><span></span><br />');
                }
            }
        }

        if (!is_single() && !post_password_required() && (comments_open() || get_comments_number())) {
            echo '<span class="comments-link">';
            comments_popup_link(
                '<span class="dashicons dashicons-admin-comments"></span><span class="screen-reader-text">no comments on %s</span>',
                '1 <span class="dashicons dashicons-admin-comments"></span><span class="screen-reader-text">comments on %s</span>',
                '% <span class="dashicons dashicons-admin-comments"></span><span class="screen-reader-text">comments on %2$s</span>'

            );
            echo '</span>';
        }
        echo '<div style="clear: both;"></div>';
    }
endif;



