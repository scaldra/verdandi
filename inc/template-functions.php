<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Verdandi
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function verdandi_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
		$classes[] = 'hfeed';
    }
    return $classes;
}
add_filter( 'body_class', 'verdandi_body_classes' );



function verdandi_comments_array( $comments ) {
    $arrComments = array();
    $arrRest = array();
    foreach( $comments as $comment ) {
        ($comment->comment_type == 'pingback' || $comment->comment_type == 'trackback') ? $arrRest[] = $comment : $arrComments[] = $comment;
        }
    return array_merge($arrComments,$arrRest);
}
add_filter( 'comments_array', 'verdandi_comments_array' );
